import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  // @Input() public isUserLoggedIn: boolean;
  isLoggedIn: boolean;
  isPopupOpened: true;
  constructor(private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
  }
  
 
}
