import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatSortModule} from '@angular/material/sort';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavbarComponent} from './navbar/navbar.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { PdfViewerModule } from "ng2-pdf-viewer";
import {RouterModule} from '@angular/router';
import {AgmCoreModule} from '@agm/core';
import {MatCardModule} from '@angular/material';
import { ButtonsModule, WavesModule, CollapseModule } from 'angular-bootstrap-md'
import {DragDropModule} from '@angular/cdk/drag-drop';
import { CandidateComponent } from './candidate/candidate.component';
import { CandidateListComponent } from './candidate/candidate-list/candidate-list.component';
//import { Ng2SearchPipeModule } from '@ng2-search-filter';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    CandidateComponent,
    CandidateListComponent
    // FormsModule,
    // ReactiveFormsModule
  ],
    imports: [
        ReactiveFormsModule,
        HttpClientModule,
        BrowserModule,
        DragDropModule,
        CommonModule,
        MatToolbarModule,
        MatGridListModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatSnackBarModule,
        MatTableModule,
        MatIconModule,
        MatSortModule,
        MatDialogModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        PdfViewerModule,
        ButtonsModule, 
        WavesModule,
        CollapseModule,
        // Ng2SearchPipeModule,
        RouterModule.forRoot([

            {path: 'navbar', component: NavbarComponent},
            {path: 'dashboard', component: DashboardComponent},
            {path: 'candidate', component: CandidateComponent},
            {path: 'candidate-list', component: CandidateListComponent},
            {path: '**', redirectTo: '/'}

        ]),
        AgmCoreModule.forRoot({
            apiKey: ''
        }),
        MatCardModule


    ],
  entryComponents: [NavbarComponent],
  exports: [
    // MatToolbarModule,
    // MatGridListModule,
    // MatFormFieldModule,
    // MatInputModule,
    // MatRadioModule,
    // MatSelectModule,
    // MatCheckboxModule,
    // MatDatepickerModule,
    // MatNativeDateModule,
    // MatButtonModule,
    // MatSnackBarModule,
    // MatTableModule,
    // MatIconModule,
    // MatSortModule,
    // MatDialogModule,

  ],
  providers: [],

  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
