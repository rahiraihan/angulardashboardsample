import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Candidate } from '../models/candidate';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {
  formData: Candidate;
  readonly rootURL = 'https://localhost:44350/api/Candidate';
  list : Candidate[];

  constructor(private http: HttpClient) { }

  postCandidate() {
    return this.http.post(this.rootURL + '/PostCandidateList', this.formData);
  }
  putPutCandidateList() {
    return this.http.put(this.rootURL + '/PutCandidateList/'+ this.formData.CandidateId, this.formData);
  }
  deletedeleteCandidate(id) {
    return this.http.delete(this.rootURL + '/deleteCandidateList/'+ id);
  }

  getCandidateData(){
    return this.http.get(this.rootURL + '/GetCandidateList');
  }
  
}