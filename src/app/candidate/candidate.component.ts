import { Component, OnInit } from '@angular/core';
import { Candidate } from './models/candidate';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent implements OnInit {
  candidateForm:FormGroup
  constructor(private fb: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {
    this.reactiveForm();
  }

  reactiveForm() {
    this.candidateForm = this.fb.group({
      CandidateId: [''],
      CandidateFullName: [''],
      CandidateMobile: [''],
      CandidateEmail: [''],
      CandidateAge: [''],
      CandidateBloodGroup: [''],
      CandidateAddress: [ '']
    });
  }

  onSubmit(){

     this.httpClient.post('https://localhost:44350/api/Candidate/PostCandidateList', this.candidateForm).subscribe(data =>{
       console.log(data);
     });
  }

}
