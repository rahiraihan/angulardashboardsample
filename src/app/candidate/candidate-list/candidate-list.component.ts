import { Component, OnInit } from '@angular/core';
import { MatDialog , MatDialogConfig} from '@angular/material';
import { CandidateComponent } from '../candidate.component';
import { CandidateService } from '../services/candidate.service';
import { Candidate } from '../models/candidate';

@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.component.html',
  styleUrls: ['./candidate-list.component.css']
})
export class CandidateListComponent implements OnInit {
  isPopupOpened: boolean;
  candidateFullList:Candidate[];

  constructor(private dialog: MatDialog, private _canService:CandidateService) { }

  ngOnInit() {
  this.getData();
    
  }

  getData(){
    this._canService.getCandidateData().subscribe( data =>{
      console.log(data);
    });
   
  }
  addDepartment() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(CandidateComponent, {
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
    //dialogConfig.width = '60px';
   
  }

  // editDepartment(id: number) {
  //   this.isPopupOpened = true;
  //   const editDepartment = this._departmentService.getDepartment().find(c => c.DepartmentId === id);
  //   const dialogRef = this.dialog.open(DepartmentComponent, {
  //     data: editDepartment
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //   });
  // }

}
