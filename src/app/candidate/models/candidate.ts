export interface Candidate{
    CandidateId : number,
    CandidateFullName: string,
    CandidateMobile: string,
    CandidateEmail: string,
    CandidateAge: number,
    CandidateBloodGroup: string,
    CandidateAddress: string,
    
 }